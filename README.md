# Revolut Demo Application


## Task
The app must download and update rates every 1 second using API
https://revolut.duckdns.org/latest?base=EUR

List all currencies you get from the endpoint (one per row). Each row has an input where you
can enter any amount of money. When you tap on currency row it should slide to top and its
input becomes first responder. When you’re changing the amount the app must simultaneously
update the corresponding value for other currencies.

Use any libraries and languages(java/kotlin) you want. Please, note that the solution should be production ready.

---------------


## Architecture
 - The application is written in Clean Architecture.
 - MVVP pattern is used for Presentation layer.
 - Communication within data layers were done with Rx
 - Data layer is design with ability to extend in mind. Ex., current solution can be extended to have local storage.
 - ViewModel communicates with UI by using LiveData
 - Dagger2 is used as Dependency Injection framework
 - RecycleView shows two different view holders to the user. Data updates are provided thought DiffUtils

