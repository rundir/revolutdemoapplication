package com.boiko.revolut.rates.di

import com.boiko.revolut.di.RatesFeatureScope
import com.boiko.revolut.network.RevolutApi
import com.boiko.revolut.rates.data.RatesRepository
import com.boiko.revolut.rates.data.RevolutRatesRepository
import com.boiko.revolut.rates.data.remote.RemoteDataSource
import dagger.Module
import dagger.Provides

@RatesFeatureScope
@Module(includes = [ViewModelModule::class])
class RatesModule {

    @Provides
    fun provideRatesRepository(remoteDataSource: RemoteDataSource): RatesRepository =
        RevolutRatesRepository(remoteDataSource)

    @Provides
    fun provideRemoteDataSource(revolutApi: RevolutApi): RemoteDataSource =
        RemoteDataSource(revolutApi)
}