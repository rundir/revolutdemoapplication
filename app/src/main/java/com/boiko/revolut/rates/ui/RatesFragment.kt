package com.boiko.revolut.rates.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView.AdapterDataObserver
import com.boiko.revolut.R
import com.boiko.revolut.RevolutDemoApp
import com.boiko.revolut.rates.di.RatesComponent
import com.boiko.revolut.rates.domain.RateItem
import com.boiko.revolut.rates.ui.adapter.OnItemClickListener
import com.boiko.revolut.rates.ui.adapter.RatesAdapter
import kotlinx.android.synthetic.main.rates_fragment.*
import javax.inject.Inject

class RatesFragment : Fragment(),
    OnItemClickListener {
    private lateinit var ratesComponent: RatesComponent

    companion object {
        fun newInstance() = RatesFragment()
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory)
            .get(RatesModel::class.java)
    }

    private val ratesAdapter = RatesAdapter(this)

    private val adapterDataObserver = object : AdapterDataObserver() {
        override fun onItemRangeMoved(fromPosition: Int, toPosition: Int, itemCount: Int) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount)
            recycleView.scrollToPosition(0)
            ratesAdapter.notifyItemChanged(0)
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        ratesComponent =
            (context.applicationContext as RevolutDemoApp).appComponent.ratesComponent().create()
        ratesComponent.inject(this)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.rates_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initAdapter()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        ratesAdapter.unregisterAdapterDataObserver(adapterDataObserver)
    }

    private fun initAdapter() {
        ratesAdapter.registerAdapterDataObserver(adapterDataObserver)
        recycleView.adapter = ratesAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel.rates.observe(this, Observer {
            when(it) {
                is SuccessState -> {
                    showSuccessState(it.rateItems)
                }
                is EmptyState -> {
                    showEmptyState()
                }
                is ErrorState -> {
                    showErrorState(it.text)
                }
                is GenericErrorState -> {
                    showGenericErrorState()
                }
            }
        })

        viewModel.init()
    }

    private fun showGenericErrorState() {
        showEmptyState()
        empty.setText(R.string.generic_error)
    }

    private fun showErrorState(errorText: String) {
        showEmptyState()
        empty.text = errorText
    }

    private fun showSuccessState(rateItems: List<RateItem>) {
        showItems()
        ratesAdapter.submitList(rateItems)
    }

    private fun showItems() {
        empty.isVisible = false
        recycleView.isVisible = true
    }

    private fun showEmptyState() {
        empty.setText(R.string.empty_text)
        empty.isVisible = true
        recycleView.isVisible = false
    }

    override fun onItemSelected(name: String, price: Double) {
        viewModel.updateBaseRate(name, price)
    }

    override fun onValueChanged(price: Double) {
        viewModel.updateBaseValue(price)
    }

}
