package com.boiko.revolut.rates.di

import com.boiko.revolut.MainActivity
import com.boiko.revolut.di.RatesFeatureScope
import com.boiko.revolut.rates.ui.RatesFragment
import dagger.Subcomponent

@RatesFeatureScope
@Subcomponent(modules = [RatesModule::class])
interface RatesComponent {
    @Subcomponent.Builder
    interface Factory {
        fun create():RatesComponent
    }

    fun inject(mainActivity: MainActivity)
    fun inject(ratesFragment: RatesFragment)
}