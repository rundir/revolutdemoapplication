package com.boiko.revolut.rates.ui.adapter

import android.view.View
import android.widget.TextView
import com.boiko.revolut.rates.domain.RateItem
import com.boiko.revolut.rates.ui.toPriceString
import kotlinx.android.synthetic.main.rate_item.view.*

class RateItemViewHolder(view: View) : BaseRateViewHolder(view) {
    var listener: OnItemClickListener? = null

    private val name: TextView = view.name
    private val value: TextView = view.value

    override fun bind(rateItem: RateItem) {
        name.text = rateItem.name
        val toPriceString = rateItem.value.toPriceString()
        value.text = toPriceString
        itemView.setOnClickListener {
            listener?.onItemSelected(rateItem.name, rateItem.value)
        }
    }
}