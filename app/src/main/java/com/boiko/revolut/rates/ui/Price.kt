package com.boiko.revolut.rates.ui


import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.*

typealias Price = Double

private val format: NumberFormat = NumberFormat.getInstance(Locale.getDefault())
private val decimalFormatSymbols: DecimalFormatSymbols = DecimalFormatSymbols.getInstance()
private val numberFormat = DecimalFormat("#0.##").apply {
    roundingMode = RoundingMode.CEILING
}

fun Price.toPriceString(): String {
    return numberFormat.format(this)
}

fun String.toDoublePrice(): Double {
    return takeIf { this.isNotBlank() }
        ?.let {
            when (decimalFormatSymbols.decimalSeparator) {
                ',' -> it.replace('.', ',')
                else -> it
            }
        }
        ?.let { format.parse(it)?.toDouble() ?: 0.0 }
        ?: 0.0
}