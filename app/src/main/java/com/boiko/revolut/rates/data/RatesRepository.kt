package com.boiko.revolut.rates.data

import io.reactivex.Observable

interface RatesRepository {
    fun getRates(base: String): Observable<List<Rate>>
}