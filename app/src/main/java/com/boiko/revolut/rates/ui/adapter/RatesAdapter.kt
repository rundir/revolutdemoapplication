package com.boiko.revolut.rates.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import com.boiko.revolut.R
import com.boiko.revolut.rates.domain.RateItem

const val EDITABLE_ITEM = 0
const val GENERIC_ITEM = 1

class RatesAdapter(private val listener: OnItemClickListener) :
    ListAdapter<RateItem, BaseRateViewHolder>(RatesDiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseRateViewHolder =
        when (viewType) {
            EDITABLE_ITEM -> {
                EditableRateItemViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.editable_rate_item,
                        parent,
                        false
                    )
                ).apply {
                    this.listener = this@RatesAdapter.listener
                }
            }
            else -> {
                RateItemViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.rate_item,
                        parent,
                        false
                    )
                ).apply {
                    this.listener = this@RatesAdapter.listener
                }
            }
        }

    override fun onBindViewHolder(holder: BaseRateViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int): Int {
        return when (position) {
            0 -> {
                EDITABLE_ITEM
            }
            else -> {
                GENERIC_ITEM
            }
        }
    }

}