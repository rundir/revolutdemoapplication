package com.boiko.revolut.rates.ui.adapter

import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.TextView
import com.boiko.revolut.rates.domain.RateItem
import com.boiko.revolut.rates.ui.toDoublePrice
import com.boiko.revolut.rates.ui.toPriceString
import kotlinx.android.synthetic.main.editable_rate_item.view.*

class EditableRateItemViewHolder(view: View) : BaseRateViewHolder(view) {
    var listener: OnItemClickListener? = null

    private val name: TextView = view.name
    private val value: EditText = view.value

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            editable?.toString()?.let {
                listener?.onValueChanged(it.toDoublePrice())
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) = Unit
    }

    override fun bind(rateItem: RateItem) {
        name.text = rateItem.name
        val toPriceString = rateItem.value.toPriceString()
        itemView.setOnClickListener(null)
        value.setText(toPriceString)
        value.addTextChangedListener(textWatcher)
        value.requestFocus()
        value.isEnabled = true
    }
}