package com.boiko.revolut.rates.ui.adapter

import androidx.recyclerview.widget.DiffUtil
import com.boiko.revolut.rates.domain.RateItem

object RatesDiffCallback : DiffUtil.ItemCallback<RateItem>() {
    override fun areItemsTheSame(oldItem: RateItem, newItem: RateItem): Boolean =
        oldItem.name == newItem.name

    override fun areContentsTheSame(oldItem: RateItem, newItem: RateItem): Boolean =
        oldItem == newItem
}