package com.boiko.revolut.rates.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.boiko.revolut.rates.data.UpdateRatesUseCase
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

const val defaultRateName = "EUR"
const val defaultRateValue = 1.0

class RatesModel @Inject constructor(private val updateUseCase: UpdateRatesUseCase) : ViewModel() {

    private var activeRateName: String = defaultRateName
    private var activeRateValue: Double = defaultRateValue

    private var _rates = MutableLiveData<ViewState>()
    val rates: LiveData<ViewState>
        get() = _rates

    private val compositeDisposable = CompositeDisposable()

    fun init() {
        updateRates(activeRateName, activeRateValue)
    }

    fun updateBaseValue(value: Double) {
        updateRates(activeRateName, value)
    }

    fun updateBaseRate(name: String, value: Double) {
        updateRates(name, value)
    }

    private fun updateRates(name: String, value: Double) {
        setRequestParams(name, value)
        compositeDisposable.clear()

        updateUseCase(name, value)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                if (it.isEmpty()) {
                    _rates.postValue(EmptyState)
                } else {
                    _rates.postValue(SuccessState(it))
                }
            }, {
                if (it.message.isNullOrBlank()) {
                    _rates.postValue(GenericErrorState)
                } else {
                    _rates.postValue(ErrorState(it.message ?: ""))
                }
            }).addTo(compositeDisposable)
    }

    private fun setRequestParams(name: String, value: Double) {
        activeRateName = name
        activeRateValue = value
    }

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.dispose()
    }

}
