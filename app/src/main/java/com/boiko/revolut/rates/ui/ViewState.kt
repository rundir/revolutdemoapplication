package com.boiko.revolut.rates.ui

import com.boiko.revolut.rates.domain.RateItem

sealed class ViewState

data class SuccessState(val rateItems:List<RateItem>): ViewState()
object EmptyState: ViewState()
data class ErrorState(val text: String): ViewState()
object GenericErrorState : ViewState()