package com.boiko.revolut.rates.data.remote

import com.boiko.revolut.network.RevolutApi
import com.boiko.revolut.rates.data.Rate
import io.reactivex.Observable
import javax.inject.Inject

class RemoteDataSource @Inject constructor(private val revolutApi: RevolutApi) {

    fun getRates(base: String): Observable<List<Rate>> {
        return revolutApi.getRates(base)
            .map { rateResponse ->
                val rates = mutableListOf<Rate>()
                if (rateResponse.rates != null) {
                    rateResponse.rates.keys.forEach {
                        rates.add(
                            Rate(
                                it,
                                rateResponse.rates[it] ?: 0.0
                            )
                        )
                    }
                }
                rates
            }
    }
}