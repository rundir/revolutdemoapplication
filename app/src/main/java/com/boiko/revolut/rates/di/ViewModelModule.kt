package com.boiko.revolut.rates.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.boiko.revolut.rates.ui.RatesModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
interface ViewModelModule {

    @Binds
    fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(RatesModel::class)
    fun ratesModel(ratesModel: RatesModel): ViewModel

}