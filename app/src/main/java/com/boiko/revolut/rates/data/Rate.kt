package com.boiko.revolut.rates.data


data class Rate (val name: String, val value: Double)