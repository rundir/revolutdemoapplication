package com.boiko.revolut.rates.ui.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.boiko.revolut.rates.domain.RateItem

abstract class BaseRateViewHolder(view: View): RecyclerView.ViewHolder(view) {
    abstract fun bind(item: RateItem)
}