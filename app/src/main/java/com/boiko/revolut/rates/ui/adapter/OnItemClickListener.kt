package com.boiko.revolut.rates.ui.adapter

interface OnItemClickListener {
    fun onItemSelected(name: String, price: Double)
    fun onValueChanged(price: Double)
}
