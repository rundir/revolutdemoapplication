package com.boiko.revolut.rates.data

import com.boiko.revolut.rates.data.remote.RemoteDataSource
import io.reactivex.Observable
import javax.inject.Inject

class RevolutRatesRepository @Inject constructor(private val remoteDataSource: RemoteDataSource) :
    RatesRepository {

    override fun getRates(base: String): Observable<List<Rate>> = remoteDataSource.getRates(base)

}