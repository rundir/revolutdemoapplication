package com.boiko.revolut.rates.data

import com.boiko.revolut.rates.domain.RateItem
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import okhttp3.internal.toImmutableList
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class UpdateRatesUseCase @Inject constructor(private val ratesRepository: RatesRepository) {

    operator fun invoke(name: String, value: Double): Observable<List<RateItem>> {
        return Observable.interval(0, 1, TimeUnit.SECONDS, Schedulers.io())
            .flatMap { ratesRepository.getRates(name) }
            .map { rates ->
                val ratesItems = mutableListOf<RateItem>()
                ratesItems.add(RateItem(name, value))
                ratesItems.addAll(
                    rates.map {
                        RateItem(
                            it.name,
                            value * it.value
                        )
                    })
                return@map ratesItems.toImmutableList()
            }
    }
}
