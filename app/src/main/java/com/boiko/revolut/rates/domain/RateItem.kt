package com.boiko.revolut.rates.domain

import com.boiko.revolut.rates.ui.Price

data class RateItem(val name: String, val value: Price)
