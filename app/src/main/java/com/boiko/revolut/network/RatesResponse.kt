package com.boiko.revolut.network

import com.google.gson.annotations.SerializedName

data class RatesResponse (
	@SerializedName("base") val base : String?,
	@SerializedName("date") val date : String?,
	@SerializedName("rates") val rates : Rates?
)

typealias Rates = Map<String, Double>