package com.boiko.revolut.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface RevolutApi {

    @GET("latest")
    fun getRates(@Query("base") currency: String): Observable<RatesResponse>
}