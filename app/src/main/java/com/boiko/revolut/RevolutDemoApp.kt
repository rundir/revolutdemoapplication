package com.boiko.revolut

import android.app.Application
import com.boiko.revolut.di.DaggerApplicationComponent

class RevolutDemoApp: Application() {

    val appComponent = DaggerApplicationComponent.create()

}