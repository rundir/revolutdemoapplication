package com.boiko.revolut.di

import com.boiko.revolut.rates.di.RatesComponent
import dagger.Module

@Module(subcomponents = [RatesComponent::class])
class SubcomponentsModule