package com.boiko.revolut.di

import com.boiko.revolut.rates.di.RatesComponent
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [ApplicationModule::class, SubcomponentsModule::class])
interface ApplicationComponent {

    fun ratesComponent(): RatesComponent.Factory
}