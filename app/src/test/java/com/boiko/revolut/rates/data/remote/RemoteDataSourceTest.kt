package com.boiko.revolut.rates.data.remote

import com.boiko.revolut.network.RatesResponse
import com.boiko.revolut.network.RevolutApi
import com.boiko.revolut.rates.data.Rate
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Test

class RemoteDataSourceTest {

    private val revolutApi: RevolutApi = mock()

    @Test
    fun `getRates returns list of rates that provided as a map from api`() {
        val remoteDataSource = RemoteDataSource(revolutApi)
        val response = mock<RatesResponse>()
        val rates = mapOf(Pair("EUR", 1.0),Pair("AUS", 3.0))
        whenever(response.rates).thenReturn(rates)
        whenever(revolutApi.getRates("base")).thenReturn(Observable.just(response))

        remoteDataSource.getRates("base").test().assertValue(listOf(Rate("EUR", 1.0), Rate("AUS", 3.0)))
    }
}