package com.boiko.revolut.rates.data

import com.boiko.revolut.rates.domain.RateItem
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.TestScheduler
import org.junit.After
import org.junit.Before
import org.junit.Test
import java.util.concurrent.TimeUnit


class UpdateRatesUseCaseTest {

    private lateinit var testScheduler: TestScheduler
    private val ratesRepository = mock<RatesRepository>()
    lateinit var useCase: UpdateRatesUseCase

    @Before
    fun setUp() {
        testScheduler = TestScheduler()
        RxJavaPlugins.setIoSchedulerHandler { testScheduler }
        useCase = UpdateRatesUseCase(ratesRepository)
    }

    @Test
    fun `execute updates rates based on provided base rate and creates list of one rate item when repository returns empty list`() {
        whenever(ratesRepository.getRates("name")).thenReturn(Observable.just(emptyList()))

        val testObservable = useCase("name", 0.1).test()

        testScheduler.advanceTimeBy(0, TimeUnit.SECONDS)
        testObservable.assertValue(listOf(RateItem("name", 0.1)))
    }

    @Test
    fun `execute updates rates and creates list of rate items when repository returns list with applied value`() {
        whenever(ratesRepository.getRates("name")).thenReturn(
            Observable.just(
                listOf(Rate("name1", 0.1), Rate("name2", 0.2))
            )
        )

        val testObservable = useCase("name", 20.0).test()

        testScheduler.advanceTimeBy(0, TimeUnit.SECONDS)
        testObservable.assertValue(
            listOf(
                RateItem("name", 20.0),
                RateItem("name1", 0.1 * 20.0),
                RateItem("name2", 0.2 * 20.0)
            )
        )
    }

    @After
    fun tearDown() = RxJavaPlugins.reset()

}