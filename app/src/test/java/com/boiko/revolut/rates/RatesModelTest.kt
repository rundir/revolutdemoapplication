package com.boiko.revolut.rates

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import com.boiko.revolut.rates.data.UpdateRatesUseCase
import com.boiko.revolut.rates.domain.RateItem
import com.boiko.revolut.rates.ui.*
import com.nhaarman.mockito_kotlin.*
import io.reactivex.Observable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import java.io.IOException


class RatesModelTest {
    @Rule
    @JvmField
    val rule = InstantTaskExecutorRule()

    private val updateRatesUseCase = mock<UpdateRatesUseCase>()
    private val observer: Observer<ViewState> = mock()

    private lateinit var ratesModel: RatesModel

    @Before
    fun setUp() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { Schedulers.trampoline() }
        RxAndroidPlugins.setMainThreadSchedulerHandler { Schedulers.trampoline() }

        ratesModel = RatesModel(updateRatesUseCase)
        ratesModel.rates.observeForever(observer)
    }

    @Test
    fun `init provides rates for default rate`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase(
                defaultRateName,
                defaultRateValue
            )
        )
            .thenReturn(Observable.just(rateItems))

        ratesModel.init()

        verify(observer).onChanged(SuccessState(rateItems))
    }

    @Test
    fun `init provides empty state if rates are empty`() {
        whenever(
            updateRatesUseCase(
                defaultRateName,
                defaultRateValue
            )
        )
            .thenReturn(Observable.just(emptyList()))

        ratesModel.init()

        verify(observer).onChanged(EmptyState)
    }

    @Test
    fun `init provides error state on non generic error`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase(
                defaultRateName,
                defaultRateValue
            )
        )
            .thenReturn(Observable.error(IOException("message")))

        ratesModel.init()

        verify(observer, never()).onChanged(SuccessState(rateItems))
        verify(observer).onChanged(ErrorState("message"))
    }

    @Test
    fun `init provides error state on error`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase(
                defaultRateName,
                defaultRateValue
            )
        )
            .thenReturn(Observable.error(IOException()))

        ratesModel.init()

        verifyError(rateItems)
    }

    @Test
    fun `updateBaseRate provides rates based on provide base`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase("base", 3.4))
            .thenReturn(Observable.just(rateItems))

        ratesModel.updateBaseRate("base",3.4)

        verify(observer).onChanged(SuccessState(rateItems))
    }

    @Test
    fun `updateBaseRate should notify error when exception occur`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase("base", 3.4))
            .thenReturn(Observable.error(IOException()))

        ratesModel.updateBaseRate("base", 3.4)

        verifyError(rateItems)
    }

    @Test
    fun `update executes usecase with default values when arguments are not provided`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase(defaultRateName, 2.3))
            .thenReturn(Observable.just(rateItems))

        ratesModel.updateBaseValue(2.3)

        verify(observer).onChanged(SuccessState(rateItems))
    }

    @Test
    fun `update should notify error when exception occur`() {
        val rateItems = listOf<RateItem>(mock(), mock())
        whenever(
            updateRatesUseCase(defaultRateName, 2.3))
            .thenReturn(Observable.error(IOException()))

        ratesModel.updateBaseValue(2.3)

        verifyError(rateItems)
    }

    private fun verifyError(rateItems: List<RateItem>) {
        verify(observer, never()).onChanged(SuccessState(rateItems))
        verify(observer).onChanged(GenericErrorState)
    }

    @After
    fun tearDown() {
        RxJavaPlugins.reset()
        RxAndroidPlugins.reset()
    }

}