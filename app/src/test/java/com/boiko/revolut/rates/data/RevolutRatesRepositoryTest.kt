package com.boiko.revolut.rates.data

import com.boiko.revolut.rates.data.remote.RemoteDataSource
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.reactivex.Observable
import org.junit.Test

class RevolutRatesRepositoryTest{

    @Test
    fun `getRates should return list of item from remote data source`() {
        val remoteDataSource = mock<RemoteDataSource>()
        val rateList = listOf<Rate>(mock(), mock())
        whenever(remoteDataSource.getRates("base")).thenReturn(Observable.just(rateList))
        val revolutRatesRepository = RevolutRatesRepository(remoteDataSource)

        revolutRatesRepository.getRates("base").test().assertValue(rateList)
    }
}